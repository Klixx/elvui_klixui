## Interface: 80300
## Author: Klix, Dlarge
## Version: 1.74
## Title: |cfffe7b2cElvUI|r |cfff960d9KlixUI|r
## Notes: A decorative edit for |cfffe7b2cElvUI|r with many awesome features!
## Notes-deDE: Eine dekorative Erweiterung für ElvUI mit einigen nützlichen Funktionen.
## RequiredDeps: ElvUI
## DefaultState: Enabled
## SavedVariables: KUIDataDB, TeamStatsDB, AddonPanelProfilesDB, AddonPanelServerDB
## SavedVariablesPerCharacter: KUIDataPerCharDB, TeamStatsPCDB, Mail_Senders, Mail_Count
## OptionalDeps: SharedMedia, AddOnSkins, DBM, BigWigs, ls_Toasts, XIV_Databar
## X-ElvVersion: 11.46


libs\load_libs.xml
locales\load_locales.xml
core\load_core.xml
media\load_media.xml
defaults\load_defaults.xml
modules\load_modules.xml
layout\layout.lua
Bindings.xm